from app import app, db, Session
from app.LinkedInProfile import LinkedInProfile
from app.DBFunctions import add_entry, get_entry, get_all_entries, remove_entry
import app.constants as const
from datetime import timedelta
from flask import request, session

app.config['SECRET_KEY'] = "HelloWorld"


@app.route('/')
def welcome():
    return 'Welcome to my LinkedIn server :)'


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user = request.json['User']
        session["user"] = user

        return 'User {0} has logged in.'.format(user)

    else:
        return 'Please add user to the server.'


@app.route('/add_profile', methods=['POST'])
def add_profile():
    """
    Adds profile to server
    :return: profile object
    """
    id_number = request.json['Id number']
    first_name = request.json['First name']
    last_name = request.json['Last name']
    company = request.json['Company']
    one_liner = request.json['One liner']

    profile = LinkedInProfile(id_number, first_name, last_name, company, one_liner)
    add_entry(db, profile)

    return profile.__repr__()


@app.route('/get_profile', methods=['GET'])
def get_profile():
    """
    Gets specific profile
    :return: a profile
    """
    id_number = request.json['Id number']
    return get_entry(id_number).__repr__()


@app.route('/get_profiles', methods=['GET'])
def get_profiles():
    """
    Gets all profiles from server
    :return: all profiles
    """
    return get_all_entries().__repr__()


@app.route('/remove_profile', methods=['POST'])
def remove_profile():
    """
    Removes profile according to its id number.
    ** Authorized only to users on AUTHORIZED_USERS.
    :return:
    """
    if "user" in session:
        if session["user"] in const.AUTHORIZED_USERS:
            id_number = request.json['Id number']
            remove_entry(id_number, db)

            return 'ID number {0} was removed.'.format(id_number)

    return 'Unauthorized action.'


@app.before_request
def before_request():
    """
    TimeOut session verifier
    :return:
    """
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=5)
    session.modified = True


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=5000)
