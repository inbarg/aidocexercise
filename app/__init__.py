from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_sessions import Session
import app.constants as const

app = Flask(__name__)
app_session = Session()

# app.secret_key = 'Hello World'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{0}.db'.format(const.TABLE_NAME)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['SESSION_SQLALCHEMY_TABLE'] = const.TABLE_NAME

db = SQLAlchemy(app)
app_session.init_app(app)





