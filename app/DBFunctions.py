from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from app.LinkedInProfile import LinkedInProfile


def add_entry(db, profile):
    """
    Adds entry to DB
    :param profile: profile object
    :param db: db to add to
    :return: True if succeed
    """

    try:
        db.session.add(profile)
        db.session.commit()

    except exc.IntegrityError:
        db.session().rollback()


def get_entry(id_number):
    """
    Get specific entry from DB
    :return: Entry if exist
    """
    try:
        profile = LinkedInProfile.query.get(id_number)
        return profile
    except SQLAlchemy.exc.NoResultFound:
        return 'No entry found.'


def get_all_entries():
    """"
    Gets all entries from DB
    :return: All entries if DB not empty
    """
    try:
        profiles = LinkedInProfile.query.all()
        return profiles
    except SQLAlchemy.exc.NoResultFound:
        return 'No entries to return.'


def remove_entry(id_number, db):
    try:
        LinkedInProfile.query.filter(LinkedInProfile.id == id_number).delete()
        db.session.commit()

    except SQLAlchemy.exc.NoResultFound:
        return 'No entry found.'
