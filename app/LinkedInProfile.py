from app import db


class LinkedInProfile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    company = db.Column(db.String, nullable=True)
    one_liner = db.Column(db.String(200), nullable=True)

    def __init__(self, id_number, first_name, last_name, company, one_liner):
        self.id = id_number
        self.first_name = first_name
        self.last_name = last_name
        self.company = company
        self.one_liner = one_liner

    def __repr__(self):
        return '<Profile details: {0} {1}, {2}, {3}, {4}>'.format(self.first_name, self.last_name,
                                                                  self.id, self.company, self.one_liner)
